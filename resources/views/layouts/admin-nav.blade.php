
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img id="img" alt="image" class="img-circle" src="{{asset('public/product/profile_small.jpg')}}" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Admin</strong>
                             </span> <span class="text-muted text-xs block">Art Developer</span> </span> </a>

                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>

            {{--<li>--}}
                {{--<a href="{{route('product.index')}}"><i class="fa fa-th-large"></i> <span class="nav-label">الرئيسيه</span></a>--}}
            {{--</li>--}}


            {{--<li>--}}

                {{--{{dd("sdsd" , $categories)}}--}}
                {{--<a href="#"><i class="fa fa-th-list"></i> <span class="nav-label">الاقسام</span> <span class="fa arrow"></span></a>--}}
                {{--<ul class="nav nav-second-level collapse">--}}
                    {{--@foreach($categories as $category)--}}
                    {{--<li><a href="{{url("/prod/{$category->id}")}}">{{$category->category_name}}</a></li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="{{url('/order')}}"><i class="fa fa-first-order"></i> <span class="nav-label">الطلبات</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="{{route('image.index')}}"><i class="fa fa-address-card"></i> <span class="nav-label">صور سليدر</span></a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="{{url('/contacts')}}"><i class="fa fa-address-card"></i> <span class="nav-label">الاتصال</span></a>--}}
            {{--</li>--}}
        </ul>

    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
<nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        {{--<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>--}}
        {{--<input value="{{request('search')}}" type="text" name="search" placeholder="Search for something..." class="form-control" >--}}

        {{--<form role="search" class="navbar-form-custom" action="">--}}
            {{--<div class="form-group">--}}
                {{--<div class="">--}}
                    {{--<div class="input-group m-b" >--}}
                        {{--<input id="m" value="{{request('search')}}" type="text" name="search" placeholder="Search for something..." class="form-control">--}}
                        {{--<span class="input-group-btn">--}}
                            {{--<button type="submit" class="btn btn-primary">search</button>--}}
                        {{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group">--}}
                {{--<button type="submit" class="btn btn-sm btn-white" style="float: left; font-size: 15px ; margin-left:-12px" >بحث</button>--}}
            {{--</div>--}}
        {{--</form>--}}


    </div>
    {{--<ul class="nav navbar-top-links navbar-right">--}}

        {{--<li class="dropdown">--}}

            {{--<ul class="dropdown-menu dropdown-messages">--}}
                {{--<li>--}}
                    {{--<div class="dropdown-messages-box">--}}
                        {{--<a href="profile.html" class="pull-left">--}}
                            {{--<img alt="image" class="img-circle" src="img/a7.jpg">--}}
                        {{--</a>--}}
                        {{--<div>--}}
                            {{--<small class="pull-right">46h ago</small>--}}
                            {{--<strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>--}}
                            {{--<small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<div class="dropdown-messages-box">--}}
                        {{--<a href="profile.html" class="pull-left">--}}
                            {{--<img alt="image" class="img-circle" src="img/a4.jpg">--}}
                        {{--</a>--}}
                        {{--<div>--}}
                            {{--<small class="pull-right text-navy">5h ago</small>--}}
                            {{--<strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>--}}
                            {{--<small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<div class="dropdown-messages-box">--}}
                        {{--<a href="profile.html" class="pull-left">--}}
                            {{--<img alt="image" class="img-circle" src="img/profile.jpg">--}}
                        {{--</a>--}}
                        {{--<div>--}}
                            {{--<small class="pull-right">23h ago</small>--}}
                            {{--<strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>--}}
                            {{--<small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<div class="text-center link-block">--}}
                        {{--<a href="mailbox.html">--}}
                            {{--<i class="fa fa-envelope"></i> <strong>Read All Messages</strong>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--<li class="dropdown">--}}

            {{--<ul class="dropdown-menu dropdown-alerts">--}}
                {{--<li>--}}
                    {{--<a href="mailbox.html">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-envelope fa-fw"></i> You have 16 messages--}}
                            {{--<span class="pull-right text-muted small">4 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<a href="profile.html">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-twitter fa-fw"></i> 3 New Followers--}}
                            {{--<span class="pull-right text-muted small">12 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<a href="grid_options.html">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-upload fa-fw"></i> Server Rebooted--}}
                            {{--<span class="pull-right text-muted small">4 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<div class="text-center link-block">--}}
                        {{--<a href="notifications.html">--}}
                            {{--<strong>See All Alerts</strong>--}}
                            {{--<i class="fa fa-angle-right"></i>--}}
                        {{--</a>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        {{--@if(auth()->guard('web')->check())--}}

            {{--<li class="c">--}}
                {{--<a href="#"  onclick="document.getElementById('logout-form').submit();">--}}
                    {{--<i class="fa fa-sign-out"></i> Log out--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--@else--}}

            {{--<li class="c">--}}
                {{--<a href="{{ route('login') }}">--}}
                    {{--<i class="fa fa-sign-out"></i> Log in--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="{{ route('register') }}">--}}
                    {{--<i class="fa fa-sign-out"></i> register--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--@elseif(auth()->guard('admin')->check())--}}

        {{--<ul class="nav navbar-top-links navbar-left">--}}

            {{--<li>--}}
                {{--<a class="ss" href="#" onclick="document.getElementById('logout-form1').submit();">--}}
                    {{--<i class="fa fa-sign-out"></i>logout--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
        {{--<form id="logout-form1" action="{{ route('logout') }}" method="get" style="display: none;">--}}
            {{--{{ csrf_field() }}--}}
        {{--</form>--}}

    {{--</ul>--}}
    {{--@endif--}}
    {{--<div class="lang">--}}
       {{--<span> <a id="arabic" href="{{url('lang/ar')}}">عربي</a></span>--}}
        {{--<span> <a id="en" href="{{url('lang/en')}}">EN</a></span>--}}
    {{--</div>--}}


</nav>
        {{--<form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">--}}
            {{--{{ csrf_field() }}--}}
        {{--</form>--}}
        @stack("styles")
            <style>
              #m  {
                   width: 300px;
                  text-align: center;
                }
              .c {
                    padding-right: 200px;
                }
              .ss{
                  margin-right: 200px;
              }
              .lang{
                  float: left;
                 /*margin-right: 500px;*/
                 margin-top: 18px;
                 font-size: 18px;
                  margin-right: 665px;
              }
                #arabic
                {
                 color: red;
                 border-left: 2px solid #1b6d85;
                    padding: 0 .5rem;
                }
                #en
                {
                    padding: 0 .5rem;
                    color: #1b1b1b;

                }
                #img{
                    width: 30px;
                    height: 30px;
                }

            </style>




