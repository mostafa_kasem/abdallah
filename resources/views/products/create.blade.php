@extends('layouts.app')


@section('content')
    <div class="ibox float-e-margins">
        <div><h1 style="margin-right: 10px">اضافه منتج جديد</h1></div>
        <br>
    <div class="ibox-content">


        @include('alerts')
        <form action="{{url('product')}}" method="post"  class="form-horizontal" enctype="multipart/form-data">
            {{csrf_field()}}

            {{--create parent_id--}}
            {{--<input type="hidden" name="parent_id"  class="form-control">--}}

            <div class="form-group"><label class="col-sm-2 control-label">اسم المنتج</label>

                <div class="col-sm-10"><input type="text" name="name" class="form-control" value="{{old('name')}}"></div>
            </div>

            <div class="form-group"><label class="col-sm-2 control-label">التصنيف الرئيسي</label>
                <div class="col-sm-10">
                    <select class="form-control m-b" name="category_id" id="mainCategory"  >
                        <option value="" disabled selected hidden>اختيار الكل</option>
                        @foreach($categories as $category)
                            <option  value="{{$category->id}}" {{ (collect(old('category_name'))->contains($category->id)) ? 'selected':'' }}>{{$category->category_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">التصنيف الفرعي</label>
                <div class="col-sm-10">
                    <select class="form-control m-b" name="branch" id="branch" >
                        <option value="" disabled selected hidden>اختيار الكل</option>

                        {{--@foreach($items as $item)--}}
                        {{--<option value="{{$item->category_name}}">{{$item->category_name}}</option>--}}
                        {{--@endforeach--}}

                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group"><label class="col-sm-2 control-label">صوره المنتج</label>

                    <div class="col-sm-10"><input type="file" name="pic" class="form-control" style="margin-right: 11px"></div>
                </div>

            </div>
            <div class="form-group"><label class="col-sm-2 control-label">سعر الشراء</label>

                <div class="col-sm-10"><input id="buy_price" type="number" name="buy_price" class="form-control" value="{{old('price')}}"></div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">سعر البيع</label>

                <div class="col-sm-10"><input type="number" name="sell_price" class="form-control" value="{{old('price')}}"></div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">الكميه</label>

                <div class="col-sm-10"><input id="quantity" type="number" name="quantity" class="form-control" value="{{old('quantity')}}"></div>
            </div>



            <div class="row">
                <div class="col-lg-12">
                    {{--<div class="ibox-title">--}}
                        <h3 style="margin-right: 0px;">وصف المنتج</h3>
                        <div class="ibox-content ">
                            <textarea class="summernote form-group" cols="10" rows="10" name="description">
                                {{old('description')}}
                            </textarea>
                        </div>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;margin-right: -190px">

                    {{--<div class="multiselect">--}}
                        <h3 class="col-sm-2 control-label" style="margin-right: 60px;">التاجات</h3>


                        <select class="js-example-basic-multiple " id="tags" name="states[]" multiple="multiple">
                            @foreach($tags as $tag)
                            <option value="{{$tag->id}}" name="tag_id[]">{{$tag->name}}</option>
                            @endforeach
                        </select>




                        {{--<div class="selectBox" onclick="showCheckboxes()">--}}
                            {{--<select>--}}
                                {{--<option>اختر من هنا</option>--}}
                            {{--</select>--}}
                        {{--<div id="checkboxes">--}}
                            {{--@foreach($tags as $tag)--}}
                            {{--<label for="one">--}}
                                {{--<input type="checkbox" value="{{$tag->id}}" name="tag_id[]">{{$tag->name}}</label>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="form-group" id="show" style="display: none" ><label class="col-sm-2 control-label" style="text-align: center;">التكلفه</label>

                <div class="col-sm-10"><input id="cost"    class="form-control"  ></div>
            </div>
            <div class="text-center">
                <button  type="submit" class="btn btn-primary btn-rounded bg-success " >
                    حفظ
                </button>
            </div>
        </form>
        <div class="row" style="margin-top: 5px;">
        </div>
    </div>
    </div>
@stop
@push('scripts')
    <!-- SUMMERNOTE -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


    <script src="{{asset('assets/js/plugins/summernote/summernote.min.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.summernote').summernote();
            $("#mainCategory").change(function (e) {
                e.preventDefault();

                var catId = $(this).val();
                // console.log(catId)
                // let data = {catId}
                axios.get('{{url('child')}}'+'/' + catId)
                    .then(function (response) {
                        console.log(response.data.status);

                        if (response.data.status == true){
                            // var tt = "" ;
                            $("#branch").empty().append(response.data.data)

                        }
                    })
                // .catch(function (error) {
                //     console.log(error);
                // })
                // .then(function () {
                //     // always executed
                // });



            });


                $('#buy_price,#quantity').on('keydown ', function() {
                    var buy_price = $('#buy_price').val();
                    var qnt = $('#quantity').val();
                    var cost = buy_price * qnt;
                        // alert(cost);

                        $("#cost").val(cost);
                        $("#show").show();

                    // });
                });


        })

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });


    </script>
@endpush


@push('styles')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <link href="{{asset('assets/css/css/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/css/plugins/summernote/summernote-bs3.css')}}" rel="stylesheet">
    <style>
        .note-editor.note-frame .note-editing-area .note-editable{
            height: 200px;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }


        #checkboxes label {
            display: block;

        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }

        .ibox-content{
            margin-right: 73px;
            background-color: ghostwhite;
        }


        .form-horizontal .form-group{
            margin-right: -124px;
        }
        .selectBox select{
            margin-right: 71px;
        }
        #tags{
            width: 247px;
        }

    </style>
@endpush
