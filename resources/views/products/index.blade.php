@extends('layouts.app')
@section('content')

    <form>

        <div class="row"style="margin-top: 10px">
            <div class="form-group"><label class="col-sm-2 control-label" style="text-align: center"> اسم المنتج او الوصف</label>

                <div class="col-sm-8"><input type="text" name="search" class="form-control" value="{{request('search')}}"></div>
            </div>
        </div>
        <div class="row"style="margin-top: 10px" >
            <div class="form-group"><label class="col-sm-2 control-label" style="text-align: center">التصنيف الرئيسي</label>

                <div class="col-sm-4"><select id="mainCategory" name="parent_category_id"   class=" form-control">
                        <option value=""> كل التصنيفات</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if(request('parent_category_id') == $category->id){{'selected'}}@endif>{{$category->category_name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group" style="margin-top: -16px"><label class="col-md-2 control-label"> التاج </label>

                <div class="col-sm-4"><input type="text" name="tag" class=" form-control"
                                              value="{{request('tag')}}"
                                              placeholder=" التاج" style="margin-right: -151px;margin-left: 213px"></div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-2 text-center " style="margin-top: 2em;margin-right: 500px">
                <button type="submit" class="btn btn-primary" style="font-size: 20px">عرض</button>
            </div>
        </div>
    </form>


    <div class="wrapper wrapper-content">
        <div class="col-md-4 col-md-push-8">
            <a type="button" class="btn btn-primary btn-rounded bg-success" href="{{route('product.create')}}">
                اضافه منتج
            </a>

        </div>
    </div>
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            @include('alerts')
            <div class="ibox-content">

                <table class="table table-bordered">
                    <thead class="thead-light">
                    <tr >
                        <th scope="col">اسم المنتج</th>
                        <th scope="col">الوصف</th>
                        <th scope="col">التصنيف </th>
                        <th scope="col">التاجات</th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">
                                {{$product->name}}
                            </th>
                            <th scope="row">
                                {{$product->description}}
                            </th>
                            <th scope="row">
                              {{( $product->category ) ? $product->category->parent['category_name']:"لا يوجد" }}
                            </th>

                            <th scope="row">
                                @foreach($product->tags as $tag)
                              {{$tag->name}},
                                @endforeach
                            </th>


                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>


        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title text-center ">
                {{$products->onEachSide(1)->appends(Request::capture()->except('page'))->render() }}
            </div>
        </div>
    </div>
@stop
@push('styles')
    <style>
        th{
            text-align: center;
        }
        .form-control{
            display: inline-block;
            width: -webkit-fill-available;
        }
    </style>
    @endpush
@push('scripts')
    <script>

        $(document).on("click",".delete", function (e) {
            e.preventDefault()
            // alert("asasa") ;
            // console.log("dsdsdss")

// alert();
            /* get data from button */
            var id = $(this).data("id");
// console.log("id",id);
            /* set action attribute to new url */
            $('#deleteform').attr('action', '{{url("product")}}' + '/' + id);
            /* open the modal */
            $('#deleteModal').modal('show');
        })
    </script>
@endpush