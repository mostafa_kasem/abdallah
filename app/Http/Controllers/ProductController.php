<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $products = Product::query();

        if($request->filled('search')){
            $products = Product::where(function ($q)use ($request){
//            $products = $products->where('tags',function ($query)use ($request){
                $q->where('name','like', '%'. $request->search .'%');
                $q->orWhere('description', 'like', '%' . $request->search . '%');
            });
        }


        if($request->filled('tag')){
            $products = $products->whereHas('tags',function ($query)use ($request){
                $query->where('name','like', '%'. $request->tag .'%');
            });
        }

        if($request->filled('parent_category_id')){
            $products = $products->whereHas('category',function ($category) use($request){
                $category->where('parent_id',$request->parent_category_id);
            });
        }

        $products = $products->paginate(5);
        $categories = Category::where('parent_id',0)->get();
        return view('products.index',compact('products','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $categories = Category::where('parent_id',0)->get();
        return view('products.create',compact('categories','items','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $items =[
            'name' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'quantity' => 'required',
            'buy_price' => 'required',
            'sell_price' => 'required',
            'quantity' => 'required',
            'tag_id' => 'required',
        ];
        $message =[
            'name.required'=>'اسم المنتج مطلوب',
            'category_id.required'=>' التصنيف الرئيسي مطلوب',
            'description.required'=>' الوصف مطلوب',
            'buy_price.required'=>' سعر الشراء مطلوب',
            'sell_price.required'=>' سعر البيع مطلوب',
            'quantity.required'=>' الكميه مطلوب',
            'pic.required'=>'صوره المنتج مطلوبه'

        ];
        $this->validate($request,$items,$message);
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->buy_price = $request->buy_price;
        $product->sell_price = $request->sell_price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->branch;

//dd($request->pic);
        if ( $request->hasFile('pic')  ) {
//            dd($request->pic);
            $image = $request->pic;
            $image_new_name = time() . $image->getClientOriginalName();
            $image->move(public_path().'/product', $image_new_name);
            $product->image = $image_new_name;

        }
        $product->save();




        $tag = $request->tag_id;
        //dd($tag);
        $product->tags()->sync($tag);
        return redirect(url('product'))->with('success','تمت الاضافه بنجاح');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function childs( $id )
    {

        $cats = Category::where("parent_id", $id)->select("id", "category_name")->get();
        $html = "";
        $html.= "<option value=''>كل التصنيفات الفرعيه</option>" ;
        foreach ($cats as $cat) {
            $html .= "<option value='" . $cat->id . "'>" . $cat->category_name . "</option>";
        }
        return response()->json(["status" => true, 'data' => $html]);

    }

    }
