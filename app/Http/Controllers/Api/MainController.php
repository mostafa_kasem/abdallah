<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use App\Tag;
use Validator;
use Illuminate\Http\Request;

class MainController extends Controller
{




    public function createProduct(Request $request)
    {
        $rules = [
            'name' => 'required',
//            'category_id' => 'required',
            'branch' => 'required',
//        'pic' => 'required',
            'buy_price' => 'required',
            'sell_price' => 'required',
            'quantity' => 'required',
            'description' => 'required',
            'tag_id' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return responseJson(0, $validator->errors()->first(), $validator->errors());
        } else {
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->buy_price = $request->buy_price;
            $product->sell_price = $request->sell_price;
            $product->quantity = $request->quantity;
            $product->category_id = $request->branch;
            $product->image = $request->pic;

            if ( $request->hasFile('pic')  ) {
//            dd($request->pic);
                $image = $request->pic;
                $image_new_name = time() . $image->getClientOriginalName();
                $image->move(public_path().'/product', $image_new_name);
                $product->image = $image_new_name;

            }




            $product->save();

//        $product = Product::create($request->all());
//        $product->save();
            $tag = $request->tag_id;
            //dd($tag);
            $product->tags()->sync($tag);

            return responseJson(1, 'تمت الاضافه بنجاح', $product);
        }
    }

    public function search(Request $request)
    {
//        dd(123);
        $products = Product::query();
        if($request->filled('name')){
            $products = $products->where('name','like','%'. $request->name .'%');
        }
        if($request->filled('description')){
            $products = $products->where('description', 'like', '%'. $request->description .'%');
        }
        if($request->filled('tag')){
            $products = $products->whereHas('tags',function ($query)use ($request){
                $query->where('name','like', '%'. $request->tag .'%');
            });
        }

        if($request->filled('parent_category_id')){
            $products = $products->whereHas('category',function ($category) use($request){
                $category->where('parent_id',$request->parent_category_id);
            });
        }
        $products = $products->paginate(5);
        if ($products->count() == 0) {
            return responseJson(0, 'Failed');
        } else {
        return responseJson(1, 'success', $products);
        }

    }
}
