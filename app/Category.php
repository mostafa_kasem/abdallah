<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Category extends Model
{
    protected $guarded = ['id'];
//    protected $table="cc";

    public function product(){
        return $this->hasMany(Product::class);
    }


    public function parent(){
        return $this->belongsTo(self::class,'parent_id');
    }

    public function childs() {
        return $this->hasMany(Category::class, 'parent_id');
    }


}
