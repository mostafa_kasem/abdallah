<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['created_at','updated_at'];

    public function product(){
        return $this->belongsToMany(Product::class);
    }


}
