<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

//use Illuminate\Database\Eloquent\SoftDeletes;



class Product extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['created_at','updated_at'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }




}
