<?php
/**
 * Created by PhpStorm.
 * User: 2M
 * Date: 05/03/2021
 * Time: 05:43 م
 */


 function responseJson($status,$msg,$data=null)
{
    $response = [
        'status' => $status,
        'message' => $msg,
        'data' => $data,
    ];
    return response()->json($response);
}